﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Tutorial
{
    class QueryResult
    {
        public List<string> Columns;
        public List<Row> Rows;

        public QueryResult()
        {
            Columns = new List<string>();
            Rows = new List<Row>();
        }
    }
    class Row
    {
        public List<object> Values;

        public Row()
        {
            Values = new List<object>();
        }
    }
}
