﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_Tutorial
{
    public partial class Form1 : Form
    {


        int index = 1;

        Question question;
        public Form1()
        {
            InitializeComponent();
            setAufgabe(index);
        }

        private void button_new_Click(object sender, EventArgs e)
        {
            index++;
            result_box.Text = "";
            picture_box.ImageLocation = "";
            label_status.Text = "";
            setAufgabe(index);
        }

        private void button_send_Click(object sender, EventArgs e)
        {
            var input = query_box.Text;
            if (checkAufgabe(input))
            {
                label_status.Text = "Richtig!";
                label_status.ForeColor = Color.Green;
            }
            else
            {
                label_status.Text = "Falsch!";
                label_status.ForeColor = Color.Red;
            }
        }

        private void setAufgabe(int id)
        {
            string baseDir = Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath);
            string imageDir = baseDir.Remove(baseDir.Length - 17) + "\\Images\\";

            this.question = DB.getNeueAufgabe(id);
            var t = DB.getTask(question.TaskId);
            if (question.Description != null)
            {
                label_frage.Text = question.Description;
                if (question.HintImageUrl != null)
                {
                    picture_box.ImageLocation = imageDir + question.HintImageUrl;
                    picture_box.Visible = true;
                }
                else
                {
                    picture_box.Visible = false;
                }
                taskImageBox.ImageLocation = imageDir + t.image_url;
                task_description.Text = t.description;
            }
            else
            {
                label_frage.Text = "";
                task_description.Text = "Sie haben alle Fragen beantwortet!";
                picture_box.Visible = false;
                taskImageBox.Visible = false;
                button_new.Enabled = false;
                button_send.Enabled = false;
            }
        }

        private bool checkAufgabe(string query)
        {
            try
            {
                var result = DB.ExecuteQuery(query);
                dataGridView1.DataSource = result;
                dataGridView1.Visible = true;
                result_box.Visible = false;
            }
            catch (System.Data.SQLite.SQLiteException e)
            {
                dataGridView1.Visible = false;
                result_box.Visible = true;
                result_box.Text = e.Message.ToString();
            }

            return DB.VerifyQuery(this.question.getSoutionQuery());

            return false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}