﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Tutorial
{
    public class Question
    {
        public string Description { get; set; }
        public string SolutionQuery { get; set; }
        public string HintImageUrl { get; set; }
        public string HintText { get; set; }
        public int TaskId { get; set; }
        public Question()
        {
        }
    }
}
