﻿namespace SQL_Tutorial
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.query_box = new System.Windows.Forms.TextBox();
            this.button_send = new System.Windows.Forms.Button();
            this.label_frage = new System.Windows.Forms.Label();
            this.picture_box = new System.Windows.Forms.PictureBox();
            this.result_box = new System.Windows.Forms.RichTextBox();
            this.button_new = new System.Windows.Forms.Button();
            this.label_status = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.task_description = new System.Windows.Forms.Label();
            this.taskImageBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picture_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // query_box
            // 
            this.query_box.Location = new System.Drawing.Point(12, 108);
            this.query_box.Name = "query_box";
            this.query_box.Size = new System.Drawing.Size(1238, 20);
            this.query_box.TabIndex = 0;
            // 
            // button_send
            // 
            this.button_send.Location = new System.Drawing.Point(1253, 108);
            this.button_send.Name = "button_send";
            this.button_send.Size = new System.Drawing.Size(75, 23);
            this.button_send.TabIndex = 1;
            this.button_send.Text = "Absenden";
            this.button_send.UseVisualStyleBackColor = true;
            this.button_send.Click += new System.EventHandler(this.button_send_Click);
            // 
            // label_frage
            // 
            this.label_frage.AutoSize = true;
            this.label_frage.Location = new System.Drawing.Point(9, 79);
            this.label_frage.Name = "label_frage";
            this.label_frage.Size = new System.Drawing.Size(70, 13);
            this.label_frage.TabIndex = 2;
            this.label_frage.Text = "Fragestellung";
            // 
            // picture_box
            // 
            this.picture_box.Location = new System.Drawing.Point(12, 137);
            this.picture_box.Name = "picture_box";
            this.picture_box.Size = new System.Drawing.Size(876, 303);
            this.picture_box.TabIndex = 3;
            this.picture_box.TabStop = false;
            // 
            // result_box
            // 
            this.result_box.Location = new System.Drawing.Point(12, 446);
            this.result_box.Name = "result_box";
            this.result_box.Size = new System.Drawing.Size(876, 403);
            this.result_box.TabIndex = 4;
            this.result_box.Text = "";
            this.result_box.Visible = false;
            // 
            // button_new
            // 
            this.button_new.Location = new System.Drawing.Point(1253, 79);
            this.button_new.Name = "button_new";
            this.button_new.Size = new System.Drawing.Size(75, 23);
            this.button_new.TabIndex = 5;
            this.button_new.Text = "Neue Frage";
            this.button_new.UseVisualStyleBackColor = true;
            this.button_new.Click += new System.EventHandler(this.button_new_Click);
            // 
            // label_status
            // 
            this.label_status.AutoSize = true;
            this.label_status.Location = new System.Drawing.Point(644, 9);
            this.label_status.Name = "label_status";
            this.label_status.Size = new System.Drawing.Size(37, 13);
            this.label_status.TabIndex = 6;
            this.label_status.Text = "Status";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 446);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(876, 403);
            this.dataGridView1.TabIndex = 7;
            this.dataGridView1.Visible = false;
            // 
            // task_description
            // 
            this.task_description.AutoSize = true;
            this.task_description.Location = new System.Drawing.Point(9, 66);
            this.task_description.Name = "task_description";
            this.task_description.Size = new System.Drawing.Size(22, 13);
            this.task_description.TabIndex = 8;
            this.task_description.Text = "Bla";
            // 
            // taskImageBox
            // 
            this.taskImageBox.Location = new System.Drawing.Point(894, 137);
            this.taskImageBox.Name = "taskImageBox";
            this.taskImageBox.Size = new System.Drawing.Size(918, 712);
            this.taskImageBox.TabIndex = 9;
            this.taskImageBox.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1824, 861);
            this.Controls.Add(this.taskImageBox);
            this.Controls.Add(this.task_description);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label_status);
            this.Controls.Add(this.button_new);
            this.Controls.Add(this.result_box);
            this.Controls.Add(this.picture_box);
            this.Controls.Add(this.label_frage);
            this.Controls.Add(this.button_send);
            this.Controls.Add(this.query_box);
            this.Name = "Form1";
            this.Text = "SQL-Tutorial";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picture_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taskImageBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox query_box;
        private System.Windows.Forms.Button button_send;
        private System.Windows.Forms.Label label_frage;
        private System.Windows.Forms.PictureBox picture_box;
        private System.Windows.Forms.RichTextBox result_box;
        private System.Windows.Forms.Button button_new;
        private System.Windows.Forms.Label label_status;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label task_description;
        private System.Windows.Forms.PictureBox taskImageBox;
    }
}

