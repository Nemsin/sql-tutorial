﻿using System;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Collections.Generic;

namespace SQL_Tutorial
{
    class DB
    {
        static string dbDir = "sql-tutorial.sqlite";

        public static Question getNeueAufgabe(int id)
        {
            string parentDir = Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath);
            var parentDirShort = parentDir.Remove(parentDir.Length - 17);
            var connStr = "Data Source=" + Path.Combine(parentDirShort, dbDir);

            var aufgabe = new Question();
            using (var conn = new SQLiteConnection(connStr))
            {
                try
                {
                    conn.Open();
                    string sql = "SELECT description, solution_query, hint_text, hint_image_url, task_id FROM question WHERE question_id = @id";
                    var cmd = new SQLiteCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@id", id);

                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (!rdr.IsDBNull(0)) aufgabe.Description = rdr.GetString(0);
                            if (!rdr.IsDBNull(1)) aufgabe.SolutionQuery = rdr.GetString(1);
                            if (!rdr.IsDBNull(2)) aufgabe.HintText = rdr.GetString(2);
                            if (!rdr.IsDBNull(3)) aufgabe.HintImageUrl = rdr.GetString(3);
                            if (!rdr.IsDBNull(4)) aufgabe.TaskId = rdr.GetInt32(4);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                conn.Close();
                return aufgabe;
            }
        }

        public static Task getTask(int id)
        {
            string parentDir = Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath);
            var parentDirShort = parentDir.Remove(parentDir.Length - 17);
            var connStr = "Data Source=" + Path.Combine(parentDirShort, dbDir);

            var task = new Task();
            using (var conn = new SQLiteConnection(connStr))
            {
                try
                {
                    conn.Open();
                    string sql = "SELECT description, image_url FROM task WHERE task_id = @id";
                    var cmd = new SQLiteCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@id", id);

                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            task.task_id = id;
                            if (!rdr.IsDBNull(0)) task.description = rdr.GetString(0);                        
                            if (!rdr.IsDBNull(1)) task.image_url = rdr.GetString(1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                conn.Close();
                return task;
            }
        }

        public static DataTable VerifyQuery(string query)
        {
            string parentDir = Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath);
            var parentDirShort = parentDir.Remove(parentDir.Length - 17);
            var connStr = "Data Source=" + Path.Combine(parentDirShort, "ap-aufgaben2.sqlite");

            var aufgabe = new Question();
            using (var conn = new SQLiteConnection(connStr))
            {
                conn.Open();

                var cmd = new SQLiteCommand(query, conn);

                cmd.CommandType = CommandType.Text;
                using (SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd))
                {
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                    }
                }              
            }

        }

        public static DataTable ExecuteQuery(string query)
        {
            string parentDir = Path.GetDirectoryName(System.Windows.Forms.Application.StartupPath);
            var parentDirShort = parentDir.Remove(parentDir.Length - 17);
            var connStr = "Data Source=" + Path.Combine(parentDirShort, "ap-aufgaben2.sqlite");

            var result = new QueryResult();
            //var columns = new List<string>();
            //var rows = new List<List<string>>();

            var aufgabe = new Question();
            using (var conn = new SQLiteConnection(connStr))
            {
                conn.Open();

                var cmd = new SQLiteCommand(query, conn);

                cmd.CommandType = CommandType.Text;
                using (SQLiteDataAdapter sda = new SQLiteDataAdapter(cmd))
                {
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        return dt;
                    }
                }                
                //cmd.Prepare();
                //cmd.ExecuteNonQuery();
                //using (SQLiteDataReader rdr = cmd.ExecuteReader())
                //{
                //    for (int i = 0; i < rdr.FieldCount; i++)
                //    {
                //        result.Columns.Add(rdr.GetName(i));
                //    }
                //    while (rdr.Read())
                //    {
                //        var row = new Row();
                //        for (int i = 0; i < rdr.FieldCount; i++)
                //        {
                //            if (!rdr.IsDBNull(i))
                //            {
                //                row.Values.Add(rdr.GetValue(i));
                //            }
                //            else
                //            {
                //                row.Values.Add("null");
                //            }
                //        }
                //        result.Rows.Add(row);
                //    }
                //}

                //}
                //return result;

            }
        }
    }
}